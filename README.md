# yaz_cmd_perl
Commandline script to retrieve data from Z39.50 servers.  
To execute the perl script the following dependencies have to be installed, or the script can be packaged into a standalone binary with Docker.
## Requires 
* [http://zoom.z3950.org/bind/perl/](http://zoom.z3950.org/bind/perl/)
  * [https://metacpan.org/release/Net-Z3950-ZOOM](https://metacpan.org/release/Net-Z3950-ZOOM)
    * `perl -MCPAN -e shell`
    * `install ZOOM`

* needs libyaz from yaz toolkit
  * [https://www.indexdata.com/resources/software/yaz/](https://www.indexdata.com/resources/software/yaz/)
  * [http://ftp.indexdata.dk/pub/yaz/yaz-5.27.1.tar.gz](http://ftp.indexdata.dk/pub/yaz/yaz-5.27.1.tar.gz)
    * `./buildconf.sh`
    * `./configure`
    * `make`
    * `sudo make install`
  * or
    * `./buildconf.sh`
    * `./configure --prefix=$HOME/myapps`
    * `make`
    * `make install`
    * ...
    * Add `export PATH="$HOME/myapps/bin:$PATH"` to `.bashrc`
    * (Reload file with `source ~/.bashrc`

## Usage
* `perl yaz_cmd.pl <host> <port> <databaseName> <user> <password> <syntax> <query> (<query_type>) (<max_results>)`  
* `perl yaz_cmd.pl --test <number_from_1-5>`  
* `syntax` is either `MAB`or `USMARC`  
* `max_results` default is 10  
* Query in `pqf` `cql` `ccl` (default) format  
    * `pqf` is parsed with help of `pqf.properties`
        * Default one is integrated
        * Example file is provided
    *  `ccl` is parsed with help of `ccl.bib`
        * Default one is integrated
        * Example file is provided



## Packaging
* `pp -o yaz_cmd_packed yaz_cmd.pl` packed **without** shared library dependencies
* `pp -M ZOOM -o packed3 yaz_cmd.pl` packed explicitly with per ZOOM module **without** shared library dependencies
* `pp -M ZOOM -l /usr/local/lib/libyaz.so -o yaz_cmd_packed yaz_cmd.pl` packed **with** shared zoom libraries. **standalone binary**
    * ( needs libc -- pack on system with lowest libc when crosscompiling ofr other machine)
    * (use provided docker scripts to package binary with specific libc version)
        * `./debian/docker_build_image.sh`
        * `./docker_build_pp.sh`