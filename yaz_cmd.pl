#! /usr/bin/env perl
use strict;
use warnings;
use ZOOM;
use File::Temp qw(tempfile);
# https://metacpan.org/release/Net-Z3950-ZOOM
# perl -MCPAN -e shell
# install ZOOM
#
# needs libyaz from yaz toolkit
#https://www.indexdata.com/resources/software/yaz/
#http://ftp.indexdata.dk/pub/yaz/yaz-5.27.1.tar.gz
#./buildconf.sh
#./configure
# (sudo) make install


# ------------------------
# Sample cql configuration
# ------------------------
my $cql_qual = << "END_MESSAGE";
# Propeties file to drive org.z3950.zing.cql.CQLNode's toPQF()
# back-end and the YAZ CQL-to-PQF converter.  This specifies the
# interpretation of various CQL indexes, relations, etc. in terms
# of Type-1 query attributes.
#
# This configuration file generates queries using BIB-1 attributes.
# See http://www.loc.gov/z3950/agency/zing/cql/dc-indexes.html
# for the Maintenance Agency's work-in-progress mapping of Dublin Core
# indexes to Attribute Architecture (util, XD and BIB-2)
# attributes.

# Identifiers for prefixes used in this file. (index.*)
set.cql         = info:srw/cql-context-set/1/cql-v1.1
set.rec         = info:srw/cql-context-set/2/rec-1.0
set.dc          = info:srw/cql-context-set/1/dc-v1.1
set.bath        = http://zing.z3950.org/cql/bath/2.0/

# default set (in query)
set             = info:srw/cql-context-set/1/dc-v1.1

# The default access point and result-set references
index.cql.serverChoice                  = 1=1016
        # srw.serverChoice is deprecated in favour of cql.serverChoice
        # BIB-1 "any"

index.rec.id                            = 1=12

index.dc.title                          = 1=4
index.dc.subject                        = 1=21
index.dc.creator                        = 1=1003
index.dc.author                         = 1=1003
        ### Unofficial synonym for "creator"
index.dc.editor                         = 1=1020
index.dc.publisher                      = 1=1018
index.dc.description                    = 1=62
        # "abstract"
index.dc.date                           = 1=30
index.dc.resourceType                   = 1=1031
        # guesswork: "Material-type"
index.dc.format                         = 1=1034
        # guesswork: "Content-type"
index.dc.resourceIdentifier             = 1=12
        # "Local number"
index.dc.source                         = 1=1019
        # "Record-source"
index.dc.language                       = 1=54
        # "Code--language"
index.dc.relation                       = 1=?
        ### No idea how to represent this
index.dc.coverage                       = 1=?
        ### No idea how to represent this
index.dc.rights                         = 1=?
        ### No idea how to represent this

# Relation attributes are selected according to the CQL relation by
# looking up the "relation.<relation>" property:
#
relation.<                           = 2=1
relation.le                             = 2=2
relation.eq                             = 2=3
relation.exact                          = 2=3
relation.ge                             = 2=4
relation.>                           = 2=5
relation.<>                               = 2=6

### These two are not really right:
relation.all                            = 2=3
relation.any                            = 2=3

# BIB-1 doesn't have a server choice relation, so we just make the
# choice here, and use equality (which is clearly correct).
relation.scr                            = 2=3

# Relation modifiers.
#
relationModifier.relevant               = 2=102
relationModifier.fuzzy                  = 2=100
        ### 100 is "phonetic", which is not quite the same thing
relationModifier.stem                   = 2=101
relationModifier.phonetic               = 2=100

# Position attributes may be specified for anchored terms (those
# beginning with "^", which is stripped) and unanchored (those not
# beginning with "^").  This may change when we get a BIB-1 truncation
# attribute that says "do what CQL does".
#
position.first                          = 3=1 6=1
        # "first in field"
position.any                            = 3=3 6=1
        # "any position in field"
position.last                           = 3=4 6=1
        # not a standard BIB-1 attribute
position.firstAndLast                   = 3=3 6=3
        # search term is anchored to be complete field

# Structure attributes may be specified for individual relations; a
# default structure attribute my be specified by the pseudo-relation
# "*", to be used whenever a relation not listed here occurs.
#
structure.exact                         = 4=108
        # string
structure.all                           = 4=2
structure.any                           = 4=2
structure.*                             = 4=1
        # phrase

# Truncation attributes used to implement CQL wildcard patterns.  The
# simpler forms, left, right- and both-truncation will be used for the
# simplest patterns, so that we produce PQF queries that conform more
# closely to the Bath Profile.  However, when a more complex pattern
# such as "foo*bar" is used, we fall back on Z39.58-style masking.
#
truncation.right                        = 5=1
truncation.left                         = 5=2
truncation.both                         = 5=3
truncation.none                         = 5=100
truncation.z3958                        = 5=104

# Finally, any additional attributes that should always be included
# with each term can be specified in the "always" property.
#
always                                  = 6=1
# 6=1: completeness = incomplete subfield


# Bath Profile support, added Thu Dec 18 13:06:20 GMT 2003
# See the Bath Profile for SRW at
#       http://zing.z3950.org/cql/bath.html
# including the Bath Context Set defined within that document.
#
# In this file, we only map index-names to BIB-1 use attributes, doing
# so in accordance with the specifications of the Z39.50 Bath Profile,
# and leaving the relations, wildcards, etc. to fend for themselves.

index.bath.keyTitle                     = 1=33
index.bath.possessingInstitution        = 1=1044
index.bath.name                         = 1=1002
index.bath.personalName                 = 1=1
index.bath.corporateName                = 1=2
index.bath.conferenceName               = 1=3
index.bath.uniformTitle                 = 1=6
index.bath.isbn                         = 1=7
index.bath.issn                         = 1=8
index.bath.geographicName               = 1=58
index.bath.notes                        = 1=63
index.bath.topicalSubject               = 1=1079
index.bath.genreForm                    = 1=1075
END_MESSAGE

# ------------------------
# Sample ccl configuration
# ------------------------
my $ccl_qual = << "END_MESSAGE";
# CCL field mappings
#
# The rule below is used when no fields are specified
term t=l,r  s=al
#
# Simple rule for a field called "clean"
clean t=l,r
#
# Rules for some BIB-1 fields
au   u=1    s=pw
ti   u=4    s=pw
isbn u=7
issn u=8
cc   u=20
su   u=21   s=pw
date u=30   r=r
dp   u=31   r=r
da   u=32   r=r
la   u=54   s=pw
ab   u=62   s=pw
note u=63   s=pw
af   u=1006 s=pw
#
# Rules for a few GILS fields
north  gils-attset,u=2040 r=o
south  gils-attset,u=2041 r=o
east  gils-attset,u=2038 r=o
west  gils-attest,u=2039 r=o
distributor gils-attset,u=2000 s=pw
distributorname gils-attset,u=2001 s=pw

# Explain fields
ExplainCategory      exp1,1=1
HumanStringLanguage  exp1,1=2
DatabaseName         exp1,1=3
TargetName           exp1,1=4
AttributeSetOID      exp1,1=5
RecordSyntaxOID      exp1,1=6
TagSetOID            exp1,1=7
ExtededServiceOID    exp1,1=8
DateAdded            exp1,1=9
DateChanged          exp1,1=10
DateExpires          exp1,1=11
ElementSetName       exp1,1=12
END_MESSAGE

# -----------------
# main cli program
# -----------------
my ($host, $port, $dbname, $user, $pw, $syntax, $query, $query_type, $max) = @ARGV;
# print "perlexecuted";
if (not defined $host && $port) {
  die "Need host and port\n";
}
else {
  if($host eq "--test" && defined $port) {
    #print "test $port \n";
    my $host2 = "";
    my $port2 = "";
    my $dbname2 = "";
    my $user2 = "";
    my $pw2 = "";
    my $syntax2 = "";
    my $query2 = "";
    my $query_type2;
    my $max2 = 2;
    if($port eq 1){
      $host2 = "193.30.112.135";
      $port2 = "9991";
      $dbname2 = "HBZ01";
      $user2 = "";
      $pw2 = "";
      $syntax2 = "MAB";
      $query2 = 'funktion';
    }
    elsif($port eq 2){
      $host2 = "bvbr.bib-bvb.de";
      $port2 = "9991";
      $dbname2 = "BVB01";
      $user2 = "";
      $pw2 = "";
      $syntax2 = "MAB";
      $query2 = "Funktion";
    }
    elsif($port eq 3){
      $host2 = "z3950.kobv.de";
      $port2 = "9991";
      $dbname2 = "U-KBV90";
      $user2 = "";
      $pw2 = "";
      $syntax2 = "USMARC";
      $query2 = "funktion";
    }
    elsif($port eq 4){
      $host2 = "sru.gbv.de";
      $port2 = "210";
      $dbname2 = "gvkplus";
      $user2 = "";
      $pw2 = "";
      $syntax2 = "USMARC";
      $query2 = '@attr 1=31 @attr 4=4 "2019"' ;
    }
    elsif($port eq 5){
      $host2 = "sru.gbv.de";
      $port2 = "210";
      $dbname2 = "gvkplus";
      $user2 = "";
      $pw2 = "";
      $syntax2 = "USMARC";
      $query2 = "funktion";
    }
    elsif($port eq 6){
      $host2 = "193.30.112.135";
      $port2 = "9991";
      $dbname2 = "HBZ01";
      $user2 = "";
      $pw2 = "";
      $syntax2 = "MAB";
      $query_type2 = "PQF";
      $query2 = '@attr 1=4 @attr 4=1 "goethe"';
    }
    elsif($port eq 7){
      $host2 = "193.30.112.135";
      $port2 = "9991";
      $dbname2 = "HBZ01";
      $user2 = "";
      $pw2 = "";
      $syntax2 = "MAB";
      $query_type2 = "CQL";
      $query2 = 'title=dinosaur';
    }
    elsif($port eq 8){
      $host2 = "193.30.112.135";
      $port2 = "9991";
      $dbname2 = "HBZ01";
      $user2 = "";
      $pw2 = "";
      $syntax2 = "MAB";
      $query_type2 = "CCL";
      $query2 = 'ti=dinosaur';
    }
    elsif($port eq 9){
      $host2 = "193.30.112.135";
      $port2 = "9991";
      $dbname2 = "HBZ01";
      $user2 = "";
      $pw2 = "";
      $syntax2 = "MAB";
      $query_type2 = "CCL";
      $query2 = 'dinosaur';
    }
    #print "tt $host2 \n";
    my $records = get_records($host2, $port2, $dbname2, $user2, $pw2, $syntax2, $query2, $query_type2, $max2);
    #print $records;
    #return $records;
  }
  else{
    if(defined $host && defined $port && defined $dbname && defined $user && defined $pw && defined $syntax && defined $query){
        if(not defined $max){
        my $max = 10;
        }
        if(not defined $query_type){
        my $query_type = 'CCL';
        }
      my $records = get_records($host, $port, $dbname, $user, $pw, $syntax, $query, $query_type, $max);
		#print $records;
		#return $records;
    }
    else {
      die "Need more arguments\n";
    }
  }
}

# ----------------
# subroutine get records from server
# ----------------
sub get_records {
  my ($host1, $port1, $dbname1, $user1, $pw1, $syntax1, $query1, $query_type1, $max1) = @_;
  #print "$host1  $port1 $dbname1 $user1 $pw1 $syntax1 $query1\n";
  if(not defined $max1){
    $max1 = 10;
    }
  if(defined $host1 && defined $port1 && defined $dbname1 && defined $user1 && defined $pw1 && defined $syntax1 && defined $query1){
#    print "$host1  $port1 $dbname1 $user1 $pw1 $syntax1 $query1, $query_type1, $max1\n";
    my $conn = new ZOOM::Connection($host1, $port1,
         databaseName => $dbname1,
         preferredRecordSyntax => uc $syntax1,
         user => $user1,
         password => $pw1,
         count => $max1,
         );
     my $rs;
     my $cqlfile;
     my $filename;
    if(defined $query_type1){
        if(uc $query_type1 eq 'CQL'){
            $rs = resultset_cql($conn, $query1);
        }
        elsif(uc $query_type1 eq 'CCL'){
            $rs = resultset_ccl($conn, $query1);
        }
        elsif(uc $query_type1 eq 'PQF'){
            $rs = resultset_pqf($conn, $query1);
        }
        else{
#            $rs = resultset_ccl($conn, $query1);
            $rs = resultset_pqf($conn, $query1);
        }
    }
    else{
#        $rs = resultset_ccl($conn, $query1);
        $rs = resultset_pqf($conn, $query1);
    }
    if(defined $cqlfile){
    $cqlfile->close;
    undef $cqlfile;
    }
    if(defined $rs){
        my $size = $rs->size();
        if(defined $size){
              if($size > $max1){
                 $size = $max1;
                 }
             print(chr(0x1c));
             for(my $pos = 0; $pos < $size; $pos++){
                my $tmp = $rs->record($pos);
                if(not defined $tmp){
                    print chr(0x00), "Can't get record for index $pos\n";
                    next;
                }
                my $rec2 = $tmp->render();
                if(not defined $rec2){
                    print chr(0x00), "Can't render for index $pos\n";
                    next;
                }
                print "$pos", "\n", $rec2;
                 if(uc $syntax1 eq "USMARC"){
                print chr(0x1d);
                }
             }
             print chr(0x1c);
             #my $rec = $rs->record(0);
             # print $rec->render();
            return $rs;
        }
    }
  }
}

# --------------
# subroutine: get ccl result set
# --------------
sub resultset_ccl {
    my ($conn, $query) = @_;
    my $rs;
    if(defined $conn && defined $query){
        if(-e "ccl.bib"){
            $conn->option(cclfile => "ccl.bib");
        }
        else{
            $conn->option(cclqual => $ccl_qual);
        }
        my $ccl_query = new ZOOM::Query::CCL2RPN($query, $conn);
        $rs = $conn->search($ccl_query);
    }
}

# --------------
# subroutine: get cql result set
# --------------
sub resultset_cql {
    my ($conn, $query) = @_;
    my $rs;
    my $cqlfile;
    my $filename;
    if(defined $conn && defined $query){
        if(-e "pqf.properties"){
            $conn->option(cqlfile => "pqf.properties");
            my $cql_query = new ZOOM::Query::CQL2RPN($query, $conn);
            $rs = $conn->search($cql_query);
        }
        else{
            ($cqlfile, $filename) = tempfile( )
                or die "$0: can't create temporary file: $!\n";
            print $cqlfile $cql_qual;
            #            print <$cqlfile>;
            $cqlfile->close;
            $conn->option(cqlfile => $filename);
            my $cql_query = new ZOOM::Query::CQL2RPN($query, $conn);
            $rs = $conn->search($cql_query);
        }
    }
    return $rs;
}

# --------------
# subroutine: get pqf result set
# --------------
sub resultset_pqf {
    my ($conn, $query) = @_;
    if(defined $conn && defined $query){
        return $conn->search_pqf($query);
    }
}